import { Routes } from '@angular/router';
import { PagamentoComponent } from '../app/pagamento/pagamento.component';

export const routes: Routes = [
    { path: 'pagamento', component: PagamentoComponent },
    { path: '', pathMatch: 'full', redirectTo: 'pagamento' },
];