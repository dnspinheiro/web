# Cadastro de Pagamentos em Angular

Projeto realizado para desafio da Capgemini.

## Install

Execute o comando `npm install` para instalar as dependências adicionadas ao projeto no package.json

## Development server

Exceute `ng serve` para rodar um servidor de desenvolvimento. Abra `http://localhost:4200/`. A aplicação automaticamente será recarregada caso tenha alteração no projeto.

## Build

Para buildar o projeto, basta executar `ng build`. Será gerado códigos para deploy no diretório `dist/`. Caso use o `--prod` trará configurações de projeto para ambiente de produção, por exemplom o environment.

## OBS

Validação em formulário sem máscara. Classe de http-service extendida para uso em componente. 


